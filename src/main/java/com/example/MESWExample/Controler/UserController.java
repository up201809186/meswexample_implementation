package com.example.MESWExample.Controler;

import com.example.MESWExample.Models.City;
import com.example.MESWExample.Models.User;
import com.example.MESWExample.Repository.CityRepository;
import com.example.MESWExample.Repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping("/User")
public class UserController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CityRepository cityRepository;

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getAll(){
        return ResponseEntity.ok(userRepository.findAll());
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity postOne(@RequestBody @Valid User user, BindingResult result){

        if(result.hasErrors())
            return  ResponseEntity.badRequest().body(result.getAllErrors());

        Optional<City> city = cityRepository.findById(user.getCity().getId()); //Get associated city
        if(!city.isPresent()) //Check if exists
            return  ResponseEntity.badRequest().body("{\"Error\": \"No city found with that specific ID, check if the city object ID corresponds to an existing city\"}");
        else
            user.setCity(city.get()); //Associating the city info

        return ResponseEntity.ok(userRepository.save(user));
    }

}

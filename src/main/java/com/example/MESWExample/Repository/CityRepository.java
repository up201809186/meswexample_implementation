package com.example.MESWExample.Repository;

import com.example.MESWExample.Models.City;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CityRepository  extends JpaRepository<City,Integer> {

}

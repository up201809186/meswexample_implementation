package com.example.MESWExample.Repository;

import com.example.MESWExample.Models.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User,Integer> {
}
